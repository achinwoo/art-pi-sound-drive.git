# ART-PI sound驱动

## ART-PI sound驱动
- 文件名drv_sound.c，drv_sound.h采用标准HAL库实现
- 支持播放时候同时录音的全双工能力(使用soft_loopback.c测试)
- 支持录音的增益，通道数，采样率的动态调整
- 修改放音设备名sound0字符叙述串speak0
- 录音设备名mic0保持不变
- SAI2录音是SLAVE方式,但是必须开启MASTER方式，保证工作
- SAI2的时钟与SPI123冲突，原因是配置PLL2时钟过高，向下延深不支持8K采样率
- 降低时钟会造成SPIFLASH通讯错误，将SPI1切换至PLL解决
- 测试代码rec_ply_test.c 
- 录音测试方法
```
msh />wavplay_sample sdcard/test.wav
wav information:
samplerate 16000
channel 2
[D/drv.sound] set speak samplerate 16000
[D/drv.sound] set speak channels 2
[D/drv.sound] set speak samplebits 16
[D/drv.sound] speak_start
[D/drv.sound] speak_stop
```
- 放音测试方法
```
msh />wavplay_sample sdcard/test.wav
wav information:
samplerate 16000
channel 2
[D/drv.sound] set speak samplerate 16000
[D/drv.sound] set speak channels 2
[D/drv.sound] set speak samplebits 16
[D/drv.sound] speak_start
[D/drv.sound] speak_stop
```
- 录音播放同时开启的使用测试方法，采用软件环回接口，产生大约几十ms延时，该时间受RT_AUDIO_RECORD_PIPE_SIZE值影响
```
msh />soft_loopback 16000 1 90
**************LOOP BACK***************
[D/drv.sound] set speak samplerate 16000
[D/drv.sound] set speak channels 1
[D/drv.sound] set speak samplebits 16
[D/drv.sound] set speak volume 90
spk config success
[D/drv.sound] set mic samplerate 16000
[D/drv.sound] set mic channels 1
[D/drv.sound] set mic samplebits 16
[D/drv.sound] set mic gain 90
[D/drv.sound] mic_start
mic config success
[D/drv.sound] speak_start
```
## 问题遗留未解决
- 配置采样率要初始化SAI2,rt_sai2_init(),不知道是否合理
- 接口命名不是很合理
- 没有单独drv_sai.c驱动
- 设备名命名模糊，目前是"mic0","speak0",或者将mic0与speak0直接命名“sound0",是否"line_in0","lineout0","phone0"命名更细化？

| 设备名 | 设备名依赖的其他设备 |
| ------ | ------ |
| mic0 | 只能单向音频采集能力 |
| sound0 | 包含mic,line_in,speaker,lineout,phone具有音频输入输出的双向能力 |
| codec0 | 包含的I2C,SPI,SAI,I2S,PDM,甚至UART设备|
- rt_device_close实际上是关闭DMA，配置了WM8988的DAC状态，没有反向初始化SAI2
- 不知道什么原因，HAL库SAI2B初始化判断返回值有错误，忽略不受影响
- 严格按照驱动的顺序开启