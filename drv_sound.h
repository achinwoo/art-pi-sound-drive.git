/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2021-01-15     achinwoo      first version
 */

#ifndef __DRV_SOUND_H_
#define __DRV_SOUND_H_


#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "wm8988.h"
#include "stm32h7xx_hal.h"

#define BSP_USING_AUDIO
#define BSP_AUDIO_SAMPLERATE 16000
#define BSP_AUDIO_MIC_GAIN 80  //0-99
#define BSP_AUDIO_SPK_VOLUME 80  //0-99

#define RECBUFFER_SIZE 160*2
#define AUDIO_BUFFER_SIZE 160*2

extern void rt_sai2_init(void);
extern void rt_sai_dma_init(SAI_HandleTypeDef* hsai);
extern int sai2b_dma_start(void);
extern int sai2b_dma_stop(void);
extern int sai2a_dma_start(void);
extern int sai2a_dma_stop(void);

#endif
