#include <rtthread.h>
#include <rtdevice.h>
#include <rthw.h>


#include "drv_sound.h"
#define DBG_TAG              "drv.sound"
#define DBG_LVL              DBG_LOG
#include <rtdbg.h>

#define CODEC_I2C_NAME  ("i2c3")

#define RX_FIFO_SIZE         (640)
#define RX_FIFO_CNT          (2)
#define RX_FIFO_BLOCK_SIZE   (RX_FIFO_SIZE / RX_FIFO_CNT)
#define VOL_MAP_ADC(x)       ((x) * 63 / 100)







SAI_HandleTypeDef hsai_BlockA1;
SAI_HandleTypeDef hsai_BlockB1;
DMA_HandleTypeDef hdma_sai1_b;
DMA_HandleTypeDef hdma_sai1_a;




ALIGN(32) int16_t play_buffer[RECBUFFER_SIZE];
ALIGN(32) int16_t recode_buffer[RECBUFFER_SIZE];

static uint8_t *rx_fifo = (uint8_t *)recode_buffer;
static uint8_t *tx_fifo = (uint8_t *)play_buffer;
static uint32_t init_sample = SAI_AUDIO_FREQUENCY_16K;
static uint32_t init_ch = SAI_MONOMODE;
static int sound_init = 0;

struct mic_device
{
    struct rt_i2c_bus_device *i2c_bus;
    struct rt_audio_device audio;
    struct rt_audio_configure record_config;
    rt_uint8_t *rx_fifo;
    rt_uint8_t volume;
};
static struct mic_device mic_dev;

struct spk_device
{
    struct rt_i2c_bus_device *i2c_bus;
    struct rt_audio_device audio;
    struct rt_audio_configure play_config;
    rt_uint8_t *tx_fifo;
    rt_uint8_t volume;
};
static struct spk_device speak_dev;


int simplevad(short* waveData, int frameSize)
{
    int i;
    int vad = 0;
    for (i = 0; i<frameSize; i++)
    {
        if (waveData[i]>0) vad += waveData[i];
        else vad -= waveData[i];
    }
    vad = vad / frameSize;
    return vad ;
}

void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
    rx_fifo = (rt_uint8_t*)recode_buffer;
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_INVALIDATE, rx_fifo, RECBUFFER_SIZE);
    rt_audio_rx_done(&mic_dev.audio, rx_fifo, RECBUFFER_SIZE);
}

void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai)
{
    rx_fifo = (uint8_t*)recode_buffer + AUDIO_BUFFER_SIZE;
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_INVALIDATE, rx_fifo, RECBUFFER_SIZE);
    rt_audio_rx_done(&mic_dev.audio, rx_fifo, RECBUFFER_SIZE);
}

void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
    tx_fifo = (uint8_t *)play_buffer;
    rt_audio_tx_complete(&speak_dev.audio);
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, tx_fifo, AUDIO_BUFFER_SIZE);
}

void HAL_SAI_TxCpltCallback(SAI_HandleTypeDef *hsai)
{
    tx_fifo = (uint8_t *)play_buffer + AUDIO_BUFFER_SIZE;
    rt_audio_tx_complete(&speak_dev.audio);
    rt_hw_cpu_dcache_ops(RT_HW_CACHE_FLUSH, tx_fifo, AUDIO_BUFFER_SIZE);
}


void DMA1_Stream2_IRQHandler(void)
{
  rt_enter_critical();
  HAL_DMA_IRQHandler(&hdma_sai1_b);
  rt_exit_critical();

}

void DMA1_Stream3_IRQHandler(void)
{
  rt_enter_critical();
  HAL_DMA_IRQHandler(&hdma_sai1_a);
  rt_exit_critical();
}



void saib_samplebits_set(uint32_t samplebits)
{
    init_sample = samplebits;
    rt_sai2_init();
}

void saib_channel_set(uint32_t ch)
{
    init_ch  = SAI_STEREOMODE;
    if(ch == 1)
    {
        init_ch  = SAI_MONOMODE;
    }
    rt_sai2_init();
}


rt_err_t sai2_b_config_init(void)
{
    rt_sai2_init();
    return RT_EOK;
}

rt_err_t sai2_b_rx_dma(void)
{
    rt_sai_dma_init(&hsai_BlockA1);
    rt_sai_dma_init(&hsai_BlockB1);
    return RT_EOK;
}
MSH_CMD_EXPORT(sai2_b_rx_dma, sai2_b_rx_dma)

rt_err_t sai2_b_init(void)
{
    /* set sai_a DMA */
    sai2_b_config_init();
    sai2_b_rx_dma();
    return RT_EOK;
}

static rt_err_t mic_getcaps(struct rt_audio_device *audio, struct rt_audio_caps *caps)
{
    LOG_D("mic_getcaps");
    return RT_EOK;
}

static rt_err_t mic_configure(struct rt_audio_device *audio, struct rt_audio_caps *caps)
{
    rt_err_t result = RT_EOK;
    struct mic_device *mic_dev;

    RT_ASSERT(audio != RT_NULL);
    mic_dev = (struct mic_device *)audio->parent.user_data;

    switch (caps->main_type)
    {
    case AUDIO_TYPE_MIXER:
    {
        switch (caps->sub_type)
        {
        case AUDIO_MIXER_VOLUME:
        {
            mic_dev->volume = caps->udata.value;
            LOG_D("set mic gain %d", mic_dev->volume);
            wm8988_set_in_valume(mic_dev->i2c_bus, caps->udata.value);
            break;
        }

        default:
            result = -RT_ERROR;
            break;
        }

        break;
    }

    case AUDIO_TYPE_INPUT:
    {
        switch (caps->sub_type)
        {
        case AUDIO_DSP_PARAM:
        {
            /* save configs */
            mic_dev->record_config.samplerate = caps->udata.config.samplerate;
            mic_dev->record_config.channels   = caps->udata.config.channels;
            mic_dev->record_config.samplebits = caps->udata.config.samplebits;
            LOG_D("set mic samplerate %d", mic_dev->record_config.samplerate);
            LOG_D("set mic channels %d", mic_dev->record_config.channels);
            LOG_D("set mic samplebits %d", mic_dev->record_config.samplebits);

            saib_samplebits_set(caps->udata.config.samplerate);
            saib_channel_set(caps->udata.config.channels);

            break;
        }

        case AUDIO_DSP_SAMPLERATE:
        {
            mic_dev->record_config.samplerate = caps->udata.config.samplerate;
            LOG_D("set mic amplerate %d", mic_dev->record_config.samplerate);
            saib_samplebits_set(caps->udata.config.samplerate);
            break;
        }

        case AUDIO_DSP_CHANNELS:
        {
            mic_dev->record_config.channels   = caps->udata.config.channels;
            LOG_D("set mic channels %d", mic_dev->record_config.channels);
            saib_channel_set(caps->udata.config.channels);
            break;
        }

        case AUDIO_DSP_SAMPLEBITS:
        {
            mic_dev->record_config.samplebits = caps->udata.config.samplebits;
            LOG_D("set mic amplerate %d", mic_dev->record_config.samplerate);
            saib_samplebits_set(caps->udata.config.samplerate);
            break;
        }

        default:
            result = -RT_ERROR;
            break;
        }

        break;
    }

    default:
        break;
    }

    return result;
}

static rt_err_t mic_init(struct rt_audio_device *audio)
{
    LOG_D("mic_init");
    if(sound_init)
    {
        return RT_EOK;
    }
    sound_init = 1;
    mic_dev.i2c_bus = rt_i2c_bus_device_find(CODEC_I2C_NAME);
    speak_dev.i2c_bus = mic_dev.i2c_bus;
    sai2_b_init();
    wm8988_init(mic_dev.i2c_bus);
    wm8988_set_out_valume(mic_dev.i2c_bus, BSP_AUDIO_SPK_VOLUME);
    wm8988_set_in_valume(speak_dev.i2c_bus, BSP_AUDIO_MIC_GAIN);

    return RT_EOK;
}

static rt_err_t mic_start(struct rt_audio_device *audio, int stream)
{
    LOG_D("mic_start");
    if (stream == AUDIO_STREAM_RECORD)
    {
        sai2b_dma_start();
    }

    return RT_EOK;
}

static rt_err_t mic_stop(struct rt_audio_device *audio, int stream)
{
    LOG_D("mic_stop");
    if (stream == AUDIO_STREAM_RECORD)
    {
        sai2b_dma_stop();
    }

    return RT_EOK;
}


static rt_err_t speaker_getcaps(struct rt_audio_device *audio, struct rt_audio_caps *caps)
{
    LOG_D("speake_getcaps");
    return RT_EOK;
}

static rt_err_t speaker_configure(struct rt_audio_device *audio, struct rt_audio_caps *caps)
{
   rt_err_t result = RT_EOK;
   struct spk_device *spk_dev;

   RT_ASSERT(audio != RT_NULL);
   spk_dev = (struct spk_device *)audio->parent.user_data;

    switch (caps->main_type)
    {
    case AUDIO_TYPE_MIXER:
    {
        switch (caps->sub_type)
        {
        case AUDIO_MIXER_VOLUME:
        {
            spk_dev->volume = caps->udata.value;
            LOG_D("set speak volume %d", spk_dev->volume);
            wm8988_set_out_valume(spk_dev->i2c_bus, caps->udata.value);
            break;
        }

        default:
            result = -RT_ERROR;
            break;
        }

        break;
    }

    case AUDIO_TYPE_OUTPUT:
    {
        switch (caps->sub_type)
        {
        case AUDIO_DSP_PARAM:
        {
            spk_dev->play_config.samplerate = caps->udata.config.samplerate;
            spk_dev->play_config.channels = caps->udata.config.channels;
            spk_dev->play_config.samplebits = caps->udata.config.samplebits;

            LOG_D("set speak samplerate %d", spk_dev->play_config.samplerate);
            LOG_D("set speak channels %d", spk_dev->play_config.channels);
            LOG_D("set speak samplebits %d", spk_dev->play_config.samplebits);
            /* TODO: channels / samplerabit */
            saib_samplebits_set(caps->udata.config.samplerate);
            saib_channel_set(caps->udata.config.channels);

            break;
        }

        case AUDIO_DSP_SAMPLERATE:
        {
            int samplerate;

            samplerate = caps->udata.config.samplerate;
            LOG_D("set speak samplerate = %d", samplerate);
            saib_samplebits_set(samplerate);
            break;
        }

        case AUDIO_DSP_CHANNELS:
        {
            spk_dev->play_config.channels   = caps->udata.config.channels;
            LOG_D("set speak channels %d", spk_dev->play_config.channels);
            saib_channel_set(caps->udata.config.channels);
            break;
        }

        default:
            break;
        }

        break;
    }

    default:
        break;
    }

    return result;
}

static rt_err_t speaker_init(struct rt_audio_device *audio)
{
    LOG_D("speak_init");
    if(sound_init)
    {
        return RT_EOK;
    }
    sound_init = 1;
    mic_dev.i2c_bus = rt_i2c_bus_device_find(CODEC_I2C_NAME);
    speak_dev.i2c_bus = mic_dev.i2c_bus;
    sai2_b_init();
    wm8988_init(mic_dev.i2c_bus);
    wm8988_set_out_valume(mic_dev.i2c_bus, BSP_AUDIO_SPK_VOLUME);
    wm8988_set_in_valume(speak_dev.i2c_bus, BSP_AUDIO_MIC_GAIN);
    return RT_EOK;
}

static rt_err_t speaker_start(struct rt_audio_device *audio, int stream)
{
    LOG_D("speak_start");
    struct spk_device *spk_dev;

    RT_ASSERT(audio != RT_NULL);
    spk_dev = (struct spk_device *)audio->parent.user_data;

    wm8988_open_dac(spk_dev->i2c_bus);
    sai2a_dma_start();
    return RT_EOK;
}

static rt_err_t speaker_stop(struct rt_audio_device *audio, int stream)
{
    LOG_D("speak_stop");
    struct spk_device *spk_dev;

    RT_ASSERT(audio != RT_NULL);
    spk_dev = (struct spk_device *)audio->parent.user_data;

    sai2a_dma_stop();
    wm8988_close_dac(spk_dev->i2c_bus);
    return RT_EOK;
}

static void speaker_buffer_info(struct rt_audio_device *audio, struct rt_audio_buf_info *info)
{
    /**
     *               TX_FIFO
     * +----------------+----------------+
     * |     block1     |     block2     |
     * +----------------+----------------+
     *  \  block_size  /
     */
    info->buffer = (uint8_t *)play_buffer;
    info->total_size = AUDIO_BUFFER_SIZE*2;
    info->block_size = AUDIO_BUFFER_SIZE;
    info->block_count = 2;
}

int rt_hw_mic_gpio_init(void)
{
    GPIO_InitTypeDef GPIO_Initure;

    __HAL_RCC_SAI2_CLK_ENABLE();

    __HAL_RCC_GPIOI_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();

    /* PI4,7,5,6 */
    GPIO_Initure.Pin=GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
    GPIO_Initure.Mode=GPIO_MODE_AF_PP;
    GPIO_Initure.Pull=GPIO_PULLUP;
    GPIO_Initure.Speed=GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_Initure.Alternate=GPIO_AF10_SAI2;

    HAL_GPIO_Init(GPIOI,&GPIO_Initure);

    /* PG10 */
    GPIO_Initure.Pin=GPIO_PIN_10;
    HAL_GPIO_Init(GPIOG,&GPIO_Initure);

    return 0;
}


static struct rt_audio_ops mic_ops =
{
    .getcaps     = mic_getcaps,
    .configure   = mic_configure,
    .init        = mic_init,
    .start       = mic_start,
    .stop        = mic_stop,
    .transmit    = RT_NULL,
    .buffer_info = RT_NULL,
};

static struct rt_audio_ops speaker_ops =
{
    .getcaps     = speaker_getcaps,
    .configure   = speaker_configure,
    .init        = speaker_init,
    .start       = speaker_start,
    .stop        = speaker_stop,
    .transmit    = RT_NULL,
    .buffer_info = speaker_buffer_info,
};

int rt_hw_sound_init(void)
{
    rt_hw_mic_gpio_init();

    rt_memset(&mic_dev, 0, sizeof(struct mic_device));
    rt_memset(&speak_dev, 0, sizeof(struct spk_device));

    /* init default configuration */
    {
        mic_dev.record_config.samplerate = BSP_AUDIO_SAMPLERATE;
        mic_dev.record_config.channels   = 1;
        mic_dev.record_config.samplebits = 16;
        mic_dev.volume                   = BSP_AUDIO_MIC_GAIN;
    }

    /* register sound device */
    mic_dev.audio.ops = &mic_ops;
    rt_audio_register(&mic_dev.audio, "mic0", RT_DEVICE_FLAG_RDONLY, &mic_dev);

    /* init default configuration */
    {
        speak_dev.play_config.samplerate = BSP_AUDIO_SAMPLERATE;
        speak_dev.play_config.channels   = 1;
        speak_dev.play_config.samplebits = 16;
        speak_dev.volume                   = BSP_AUDIO_SPK_VOLUME;
    }
    speak_dev.audio.ops = &speaker_ops;
    rt_audio_register(&speak_dev.audio, "speak0", RT_DEVICE_FLAG_WRONLY, &speak_dev);

    return RT_EOK;
}
INIT_DEVICE_EXPORT(rt_hw_sound_init);
MSH_CMD_EXPORT(rt_hw_sound_init, rt_hw sound init);


void rt_sai_dma_init(SAI_HandleTypeDef* hsai)
{

    __HAL_RCC_DMA1_CLK_ENABLE();

//    __HAL_RCC_SAI2_CLK_ENABLE();
    /* SAI1 */
    if(hsai->Instance==SAI2_Block_A)
    {

    /* Peripheral DMA init*/

    hdma_sai1_a.Instance = DMA1_Stream3;
    hdma_sai1_a.Init.Request = DMA_REQUEST_SAI2_A;
    hdma_sai1_a.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_sai1_a.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_sai1_a.Init.MemInc = DMA_MINC_ENABLE;
    hdma_sai1_a.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_sai1_a.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    hdma_sai1_a.Init.Mode = DMA_CIRCULAR;
    hdma_sai1_a.Init.Priority = DMA_PRIORITY_LOW;
    hdma_sai1_a.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_sai1_a) != HAL_OK)
    {
//      Error_Handler();
    }


    /* Several peripheral DMA handle pointers point to the same DMA handle.
     Be aware that there is only one channel to perform all the requested DMAs. */
    __HAL_LINKDMA(hsai,hdmarx,hdma_sai1_a);

    __HAL_LINKDMA(hsai,hdmatx,hdma_sai1_a);
        /* DMA1_Stream0_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);

    }
    if(hsai->Instance==SAI2_Block_B)
    {
    /* Peripheral DMA init*/

    hdma_sai1_b.Instance = DMA1_Stream2;
    hdma_sai1_b.Init.Request = DMA_REQUEST_SAI2_B;
    hdma_sai1_b.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_sai1_b.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_sai1_b.Init.MemInc = DMA_MINC_ENABLE;
    hdma_sai1_b.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_sai1_b.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    hdma_sai1_b.Init.Mode = DMA_CIRCULAR;
    hdma_sai1_b.Init.Priority = DMA_PRIORITY_LOW;
    hdma_sai1_b.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_sai1_b) != HAL_OK)
    {
//      Error_Handler();
    }

    /* Several peripheral DMA handle pointers point to the same DMA handle.
     Be aware that there is only one channel to perform all the requested DMAs. */
    __HAL_LINKDMA(hsai,hdmarx,hdma_sai1_b);
    __HAL_LINKDMA(hsai,hdmatx,hdma_sai1_b);

    /* DMA1_Stream1_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream2_IRQn);

    }
}



void rt_sai2_init(void)
{
  hsai_BlockA1.Instance = SAI2_Block_A;
  hsai_BlockA1.Init.Protocol = SAI_FREE_PROTOCOL; //设置SAI1协议为:自由协议(支持I2S/LSB/MSB/TDM/PCM/DSP等协议)
  hsai_BlockA1.Init.AudioMode = SAI_MODEMASTER_TX;//mode:工作模式,可以设置:SAI_MODEMASTER_TX/SAI_MODEMASTER_RX/SAI_MODESLAVE_TX/SAI_MODESLAVE_RX
  hsai_BlockA1.Init.DataSize = SAI_DATASIZE_16;  //设置数据大小
  hsai_BlockA1.Init.FirstBit = SAI_FIRSTBIT_MSB; //数据MSB位优先
  hsai_BlockA1.Init.ClockStrobing = SAI_CLOCKSTROBING_RISINGEDGE; //数据在时钟的上升/下降沿选通
  hsai_BlockA1.Init.Synchro = SAI_ASYNCHRONOUS;  //音频模块异步
  hsai_BlockA1.Init.OutputDrive = SAI_OUTPUTDRIVE_ENABLE; //立即驱动音频模块输出
  hsai_BlockA1.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE; //使能主时钟分频器(MCKDIV)
  hsai_BlockA1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_1QF;  //设置FIFO阈值,1/4 FIFO
  hsai_BlockA1.Init.AudioFrequency = init_sample;//SAI_AUDIO_FREQUENCY_16K; //采样率
  hsai_BlockA1.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
  hsai_BlockA1.Init.MonoStereoMode = init_ch;//SAI_MONOMODE;//SAI_STEREOMODE;//
  hsai_BlockA1.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockA1.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  hsai_BlockA1.Init.PdmInit.Activation = DISABLE;
  hsai_BlockA1.Init.PdmInit.MicPairsNbr = 0;
  hsai_BlockA1.Init.PdmInit.ClockEnable = SAI_PDM_CLOCK1_ENABLE;

  hsai_BlockA1.FrameInit.FrameLength = 32;  //设置帧长度为32,左通道16个SCK,右通道16个SCK.
  hsai_BlockA1.FrameInit.ActiveFrameLength = 16; //设置帧同步有效电平长度,在I2S模式下=1/2帧长.
  hsai_BlockA1.FrameInit.FSDefinition = SAI_FS_CHANNEL_IDENTIFICATION; //FS信号为SOF信号+通道识别信号
  hsai_BlockA1.FrameInit.FSPolarity = SAI_FS_ACTIVE_LOW;  //FS低电平有效(下降沿)
  hsai_BlockA1.FrameInit.FSOffset = SAI_FS_BEFOREFIRSTBIT;//SAI_FS_FIRSTBIT;//  //在slot0的第一位的前一位使能FS,以匹配飞利浦标准
  hsai_BlockA1.SlotInit.FirstBitOffset = 0; //slot偏移(FBOFF)为0
  hsai_BlockA1.SlotInit.SlotSize = SAI_SLOTSIZE_16B;  //slot大小为16位
  hsai_BlockA1.SlotInit.SlotNumber = 2;  //slot数为2个
  hsai_BlockA1.SlotInit.SlotActive = 0x00000003; //使能slot0和slot1
  HAL_SAI_Init(&hsai_BlockA1);

  hsai_BlockB1.Instance = SAI2_Block_B;
  hsai_BlockB1.Init.Protocol = SAI_FREE_PROTOCOL;
  hsai_BlockB1.Init.AudioMode = SAI_MODESLAVE_RX;
  hsai_BlockB1.Init.DataSize = SAI_DATASIZE_16;
  hsai_BlockB1.Init.FirstBit = SAI_FIRSTBIT_MSB;
  hsai_BlockB1.Init.ClockStrobing = SAI_CLOCKSTROBING_RISINGEDGE;
  hsai_BlockB1.Init.Synchro = SAI_SYNCHRONOUS;
  hsai_BlockB1.Init.OutputDrive = SAI_OUTPUTDRIVE_ENABLE;
  hsai_BlockB1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_1QF;
  hsai_BlockB1.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
  hsai_BlockB1.Init.MonoStereoMode = init_ch;//SAI_MONOMODE;//SAI_STEREOMODE;//
  hsai_BlockB1.Init.CompandingMode = SAI_NOCOMPANDING;
  hsai_BlockB1.Init.TriState = SAI_OUTPUT_NOTRELEASED;
  hsai_BlockB1.Init.PdmInit.Activation = DISABLE;
  hsai_BlockB1.Init.PdmInit.MicPairsNbr = 0;
  hsai_BlockB1.Init.PdmInit.ClockEnable = SAI_PDM_CLOCK1_ENABLE;
  hsai_BlockB1.FrameInit.FrameLength = 32;
  hsai_BlockB1.FrameInit.ActiveFrameLength = 16;
  hsai_BlockB1.FrameInit.FSDefinition = SAI_FS_CHANNEL_IDENTIFICATION;
  hsai_BlockB1.FrameInit.FSPolarity = SAI_FS_ACTIVE_LOW;
  hsai_BlockB1.FrameInit.FSOffset = SAI_FS_BEFOREFIRSTBIT;
  hsai_BlockB1.SlotInit.FirstBitOffset = 0;
  hsai_BlockB1.SlotInit.SlotSize = SAI_SLOTSIZE_16B;
  hsai_BlockB1.SlotInit.SlotNumber = 2;
  hsai_BlockB1.SlotInit.SlotActive = 0x00000003;

  HAL_SAI_Init(&hsai_BlockB1);
}
MSH_CMD_EXPORT(rt_sai2_init, MX_SAI2 Init)

int sai2b_dma_start(void)
{

    if(HAL_OK != HAL_SAI_Transmit_DMA(&hsai_BlockA1, (uint8_t *)play_buffer, AUDIO_BUFFER_SIZE))
    {
//      Error_Handler();
    }

    rt_thread_delay(1);

    if(HAL_OK != HAL_SAI_Receive_DMA(&hsai_BlockB1, (uint8_t*)recode_buffer, AUDIO_BUFFER_SIZE))
    {
//      Error_Handler();
    }
    return 0;
}

MSH_CMD_EXPORT(sai2b_dma_start, sai2b dma start)

int sai2b_dma_stop(void)
{
    if(HAL_OK != HAL_SAI_DMAStop(&hsai_BlockA1))
    {
//      Error_Handler();
    }

    rt_thread_delay(1);

    if(HAL_OK != HAL_SAI_DMAStop(&hsai_BlockB1))
    {
//      Error_Handler();
    }
    return 0;
}

MSH_CMD_EXPORT(sai2b_dma_stop, sai2b dma stop)

int sai2a_dma_start(void)
{

    if(HAL_OK != HAL_SAI_Transmit_DMA(&hsai_BlockA1, (uint8_t *)play_buffer, AUDIO_BUFFER_SIZE))
    {
//      Error_Handler();
    }

    return 0;
}

MSH_CMD_EXPORT(sai2a_dma_start, sai2a dma start)

int sai2a_dma_stop(void)
{
    if(HAL_OK != HAL_SAI_DMAStop(&hsai_BlockA1))
    {
//      Error_Handler();
    }

    return 0;
}

MSH_CMD_EXPORT(sai2a_dma_stop, sai2a dma stop)


